package br.com.cliente.cliente.controller;

import br.com.cliente.cliente.model.Cliente;
import br.com.cliente.cliente.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente criarCliente(@RequestBody @Valid Cliente cliente){
        Cliente clienteObjeto = clienteService.criarCliente(cliente);
        return clienteObjeto;
    }

    @GetMapping("/{id}")
    public Cliente buscarPorId(@PathVariable(name = "id") int id){
        try{
            Cliente cliente = clienteService.buscarPorId(id);
            return cliente;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }
}