package br.com.cliente.cliente.repository;

import br.com.cliente.cliente.model.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {
}
